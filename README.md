# Python-test script

## converter.py

This script will convert either **text** or **binary** files with keywords, de-duplicates them and sorts them into the output file

Usage:

```
converter.py <input-file> <read-mode> <output-file>
```

|argument|description|
|---|---|
|**input-file**|File to be processed|
|**read-mode**|can be either "T" for Text mode or "B" for binary mode|
|**output-file**|Output file|

# Examples & Tests:

### Text files
```
python converter.py test-files/text.txt T test-files/output-text.txt
```
### Large text files (12k+ keywords)
```
python converter.py test-files/large.txt T test-files/output-large.txt
```
### Binary files
```
python converter.py test-files/binary.bin B test-files/output-binary.txt
```