@echo off

echo --- Test #1 - Testing small text file
python converter.py test-files/text.txt T test-files/output-text.txt
if errorlevel 1 ( 
  echo There was an error.
) ELSE ( 
  echo --- Test #1 OK
  echo -
)

echo --- Test #2 - Testing binary file
python converter.py test-files/text.txt T test-files/output-text.txt
if errorlevel 1 ( 
  echo There was an error.
) ELSE ( 
  echo --- Test #2 OK
  echo -
)

echo --- Test #3 - Testing large text file
python converter.py test-files/text.txt T test-files/output-text.txt
if errorlevel 1 ( 
  echo There was an error.
) ELSE ( 
  echo --- Test #3 OK
  echo -
)

echo --- Test #4 - Invalid directory / file
python converter.py test-file/text.txt T test-files/output.txt
if errorlevel 1 ( 
  echo There was an error.
) ELSE ( 
  echo --- Test #4 OK
  echo -
)