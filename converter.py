#!/usr/bin/env python3

__author__ = ['Michal Axmann']
__date__ = '2018.11.07'

import re
import sys
import struct

msg = "hello world"
lowerizer = lambda x: x.lower()

input_file = sys.argv[1]
file_mode = sys.argv[2]
output_file = sys.argv[3]
content = []

if file_mode != "T" and file_mode != "B":
  print("invalid read mode. Please fix the <file_mode> argument")
  sys.exit(1)

if file_mode == "T":
  print("reading " + input_file+ " in TEXT mode")
  try:
    f = open(input_file,"r") 
    if f.mode == 'r':
      content = f.read()
      content = content.replace("\n", ",").replace("\r", ",")  # remove all newlines
      content = re.sub(r', +', ',', content) # remove all extra spaces
      content = content.replace(",,", ",")  # remove all empty elements      
      content = content.split(",") # split to list

  except:
    print("File reading error")
    sys.exit(1)    
else:  
    print("reading " + input_file+ " in BINARY mode")
    item_lengths = []
    try:    
      f = open(input_file, "rb")
      # read the number of keywords
      total_term_bytes = f.read(4)
      total_terms = struct.unpack('>L', total_term_bytes)
      
      # read the length of each keyword
      for x in range(total_terms[0]):
        item_length_bytes = f.read(2)
        item_length = struct.unpack('>H', item_length_bytes)
        item_lengths.append(item_length[0])
      
      # read keywords
      for x in range(len(item_lengths)):
        term_bytes = f.read(item_lengths[x])
        term = term_bytes.decode("utf-8")
        content.append(term)
    except:
      print("File reading error")
      sys.exit(1)
        
content = list(map(lowerizer, content)) # all items to lowercase

content = list(set(content)) # remove duplicates
content.sort() # sort all items

print("writing output file: " + output_file)
try:
  with open(output_file, 'w+') as f:
    for i in content:
      f.write('%s\n' % i)
except:
  sys.exit(1)

sys.exit(0)